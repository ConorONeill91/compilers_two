import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by Conor on 4/12/2015.
 */
public class VariableCheckVisitor implements BasicLParserVisitor {

    /*
    * Semantic Checks in this file:
    * Check assignments are of correct type
    * Check if integer arithmetic values are constants or variables
    * Check if bool op values are constants or variables.*/

    private static HashMap<String,String> varDeclarationTypes = new HashMap<String, String>();
    private static HashMap<String,String> constDeclarationTypes = new HashMap<String, String>();

    @Override
    public Object visit(SimpleNode node, Object data) {
        node.childrenAccept(this,data); return null;
    }

    @Override
    public Object visit(ASTprogram node, Object data) {
        node.childrenAccept(this,data);
        return null;
    }

    @Override
    public Object visit(ASTvar_decl node, Object data) {
        node.childrenAccept(this,data);
        //Get var name
        SimpleNode name =  (SimpleNode)(node.jjtGetChild(0));
        SimpleNode nameToken = (SimpleNode)name.jjtGetChild(0);
        Token nameValue = (Token)nameToken.jjtGetValue();
        // Get var type
        Token type = (Token) ((SimpleNode) node.jjtGetChild(1)).jjtGetValue();
        // Put into map
       varDeclarationTypes.put(nameValue.image,type.image);

                return null;
    }

    @Override
    public Object visit(ASTconst_decl node, Object data) {
        node.childrenAccept(this,data);
        // Get const name / type
        Token name = (Token) ((SimpleNode) node.jjtGetChild(0)).jjtGetValue();
        Token type = (Token) ((SimpleNode) node.jjtGetChild(1)).jjtGetValue();
        // Put into Map
        constDeclarationTypes.put(name.image,type.image);

        return null;
    }

    @Override
    public Object visit(ASTfunction node, Object data) {
        node.childrenAccept(this,data); return null;
    }

    @Override
    public Object visit(ASTFunctionCall node, Object data) {
        node.childrenAccept(this,data); return null;
    }

    @Override
    public Object visit(ASTparam_list node, Object data) {
        node.childrenAccept(this,data); return null;
    }

    @Override
    public Object visit(ASTtype node, Object data) {
        node.childrenAccept(this,data); return null;
    }

    @Override
    public Object visit(ASTmain_prog node, Object data) {
        node.childrenAccept(this,data); return null;
    }

    @Override
    public Object visit(ASTstatement node, Object data) {
        node.childrenAccept(this,data);

      return null;
    }

    @Override
    public Object visit(ASTassignment node, Object data) {
        node.childrenAccept(this,data);
      // System.out.println("Check assignments are of correct type of both sides of the assign sign");
        Token left = (Token) ((SimpleNode) node.jjtGetChild(0)).jjtGetValue();
        Token right = (Token) ((SimpleNode) node.jjtGetChild(1)).jjtGetValue();
       // System.out.println(right.image);

        // Semantic Check for checking assignments are of the correct type.
        if(varDeclarationTypes.get(left.image) != null) {
            String leftSideType = (varDeclarationTypes.get(left.image));
            if (leftSideType.equals("bool")) {
                if (right.image.equals("true") | right.image.equals("false")) {
                    System.out.println("Correct types for Bool assignment");
                } else {
                    System.out.println("Incorrect types for Bool assignment");
                }

            }
            if (leftSideType.equals("int")) {
                try {
                    int righthand = Integer.parseInt(right.image);
                    System.out.println("Correct types for int assignment");
                } catch (NumberFormatException e) {
                    System.out.println("Incorrect types for int assignment");
                }
            }



        }

        //System.out.println("Right side " + right);
        return null;
    }


    @Override
    public Object visit(ASTaddExpr node, Object data) {
        node.childrenAccept(this,data);
        Token left = (Token) ((SimpleNode) node.jjtGetChild(0)).jjtGetValue();
        Token right = (Token) ((SimpleNode) node.jjtGetChild(1)).jjtGetValue();
        System.out.println();
        System.out.println("Add op: Left child: "+left.image + " Right child: "+ right.image);
        if(constDeclarationTypes.get(left.image)!= null) {
            System.out.println("Left child of add op is a constant");
        }else{
            System.out.println("Left child of add op is a variable");
        }
        if(constDeclarationTypes.get(right.image)!= null){
            System.out.println("Right child of add op is a constant");
        }else{
            System.out.println("Right child of add op is a variable");
        }
        return null;
    }

    @Override
    public Object visit(ASTminusExpr node, Object data) {
        node.childrenAccept(this,data);
        Token left = (Token) ((SimpleNode) node.jjtGetChild(0)).jjtGetValue();
        Token right = (Token) ((SimpleNode) node.jjtGetChild(1)).jjtGetValue();
        System.out.println();
        System.out.println("Minus op: Left child: "+left.image + " Right child: "+ right.image);
        if(constDeclarationTypes.get(left.image)!= null) {
            System.out.println("Left child of minus op is a constant");
        }else{
            System.out.println("Left child of minus op is a variable");
        }
        if(constDeclarationTypes.get(right.image)!= null){
            System.out.println("Right child of minus op is a constant");
        }else{
            System.out.println("Right child of minus op is a variable");
        }
        return null;
    }

    @Override
    public Object visit(ASTmultExpr node, Object data) {
        node.childrenAccept(this,data);
        Token left = (Token) ((SimpleNode) node.jjtGetChild(0)).jjtGetValue();
        Token right = (Token) ((SimpleNode) node.jjtGetChild(1)).jjtGetValue();
        System.out.println();
        System.out.println("Mult op: Left child: "+left.image + " Right child: "+ right.image);
        if(constDeclarationTypes.get(left.image)!= null) {
            System.out.println("Left child of mult op is a constant");
        }else{
            System.out.println("Left child of mult op is a variable");
        }
        if(constDeclarationTypes.get(right.image)!= null){
            System.out.println("Right child of mult op is a constant");
        }else{
            System.out.println("Right child of mult op is a variable");
        }return null;
    }

    @Override
    public Object visit(ASTdivExpr node, Object data) {
        node.childrenAccept(this,data);
        Token left = (Token) ((SimpleNode) node.jjtGetChild(0)).jjtGetValue();
        Token right = (Token) ((SimpleNode) node.jjtGetChild(1)).jjtGetValue();
        System.out.println();
        System.out.println("Div op: Left child: "+left.image + " Right child: "+ right.image);
        if(constDeclarationTypes.get(left.image)!= null) {
            System.out.println("Left child of div op is a constant");
        }else{
            System.out.println("Left child of div op is a variable");
        }
        if(constDeclarationTypes.get(right.image)!= null){
            System.out.println("Right child of div op is a constant");
        }else{
            System.out.println("Right child of div op is a variable");
        }return null;
    }

    @Override
    public Object visit(ASTbool node, Object data) {
        //node.dump("Bool");
        node.childrenAccept(this,data); return null;
    }

    @Override
    public Object visit(ASTnumber node, Object data) {
        node.childrenAccept(this,data); return null;
    }

    @Override
    public Object visit(ASTcondition node, Object data) {
        node.childrenAccept(this,data); return null;
    }

    @Override
    public Object visit(ASTident_list node, Object data) {
        node.childrenAccept(this,data); return null;
    }



    @Override
    public Object visit(ASTarg_list node, Object data) {
        node.childrenAccept(this,data); return null;
    }

    @Override
    public Object visit(ASTidentifier node, Object data) {
        node.childrenAccept(this,data); return null;
    }
}
