// ** SECTION 1 - OPTIONS **
options{
    IGNORE_CASE = true;
    MULTI = true;
    VISITOR = true;
}

// ** SECTION 2 - USER CODE **
PARSER_BEGIN(BasicLParser)

import java.util.*;
import java.io.*;

public class BasicLParser {

    public static HashTable ST = new HashTable();

    public static void main(String [] args) throws FileNotFoundException, ParseException{
        String temp;
        STC temp2;

        BasicLParser parser;

        if(args.length < 1){
            System.out.println("Please pass in a filename.");
            System.exit(1);
        }
        parser = new BasicLParser(new java.io.FileInputStream(args[0]));
        SimpleNode e = parser.program();
        System.out.println("AST");
        e.dump(">");

        System.out.println();
        System.out.println("Symbol Table");
        Enumeration t = ST.keys();
        while(t.hasMoreElements()){

        }

    }
}

PARSER_END(BasicLParser)
// ** SECTION 3 - TOKEN DEFINITIONS & SKIPS **

TOKEN_MGR_DECLS :
{
    static int commentNesting = 0;
}

SKIP : /* COMMENTS */
{"/*" { commentNesting++; } : IN_COMMENT

}
<IN_COMMENT> SKIP :
{       "/*" { commentNesting++; }
    | "*/" { commentNesting--;
            if (commentNesting == 0)
                SwitchTo(DEFAULT);
           }
    | <~[]>
}

SKIP : // SINGLE LINE COMMENTS
{
    <SINGLE_LINE_COMMENT : "--" (~["\n", "\r"])* ("\r" | "\n" | "\r\n") >
}

SKIP : // IGNORE SPACES, TABS, NEWLINES, FORM FEEDS & CARRIAGE RETURNS
{
            " "
        |   "\t"
        |   "\n"
        |   "\f"
        |   "\r"
}

TOKEN : // KEYWORDS
{
        <AND : "and">
    |   <BOOL : "bool">
    |   <CONST : "const">
    |   <DO : "do">
    |   <ELSE : "else">
    |   <FALSE : "false">
    |   <IF : "if">
    |   <INT : "int">
    |   <MAIN : "main">
    |   <NOT : "not">
    |   <OR : "or">
    |   <RETURN : "return">
    |   <THEN : "then">
    |   <TRUE : "true">
    |   <VAR : "var">
    |   <VOID : "void">
    |   <WHILE : "while">
    |   <BEGIN : "begin">
    |   <END : "end">
}

TOKEN : // OPERATORS & RELATIONS
{
        <PLUS : "+">
    |   <MINUS : "-">
    |   <MULTIPLY : "*">
    |   <DIVIDE : "/">
    |   <EQUALS : "=">
    |   <NOT_EQUALS : "!=">
    |   <LESS_THAN : "<">
    |   <GREATER_THAN : ">">
    |   <LESS_THAN_OR_EQUAL : "<=">
    |   <GREATER_THAN_OR_EQUAL : ">=">
    |   <LBR : "(">
    |   <RBR : ")">
    |   <COMMA : ",">
    |   <SEMI_COLON : ";">
    |   <COLON : ":">
    |   <ASSIGNMENT : ":=">

}

TOKEN : // DECIMAL NUMBER
{
    <NUMBER : (["0" - "9"])+>
}

TOKEN : // IDENTIFIER
{
        <ID : <LETTER> (<LETTER> | <DIGIT> | <UNDERSCORE>)*>
    |   <#DIGIT : ["0" - "9"]>
    |   <#LETTER : ["a" - "z","A" - "Z"]>
    |   <#UNDERSCORE : "_">
}

TOKEN : // CATCH ANYTHING ELSE
{
    <OTHER : ~[]>
}

// SECTION 4 - **GRAMMAR PRODUCTION RULES **

void identifier() : {Token t;} {
    t = <ID> {jjtThis.value = t.image; return t.image;}
}

void number() : {Token t;} {
    t = <NUMBER> {jjtThis.setNum(Integer.parseInt(t.image));}
}

SimpleNode program() : {} {
    (decl())* (function())* main_prog() <EOF> {return jjtThis;}
}

void decl() #void : {} {
    ( var_decl() | const_decl() )
}

void var_decl() #void : {} {
    <VAR> ident_list() <COLON> type() ( <COMMA> ident_list() <COLON> type() )* <SEMI_COLON> #varDecl
}

void const_decl() #void : {} {
    <CONST> identifier() <COLON> type() <EQUALS> expression() ( <COMMA> identifier() <COLON> type() <EQUALS> expression() )* <SEMI_COLON> #constDecl
}

void function () #void : {} {
     type() identifier() <LBR> param_list() <RBR> <BEGIN> ( decl() )*
     ( statement() <SEMI_COLON> )*
     <RETURN> (expression() | {}) <SEMI_COLON>
     <END>
}

void param_list() #void : {} {
    ( identifier() <COLON> type() (<COMMA> <ID> <COLON> type() )* | {} )
}

void type() #void : {String name; Token t;} {
    t = <INT> name = identifier(){jjtThis.value = t.image; ST.put(name, new STC("Int", name));} #TypeDecl(1)
|   t = <BOOL> name = identifier(){jjtThis.value = t.image; ST.put(name, new STC("Bool", name));} #TypeDecl(1)
|   t = <VOID> name = identifier(){jjtThis.value = t.image; ST.put(name, new STC("Void", name));} #TypeDecl(1)
}

void main_prog() #void : {} {
    <MAIN> <BEGIN> ( decl() )* ( statement() <SEMI_COLON> )* <END>
}

void statement() #void : {} {
     	identifier() statementFac()
    | 	<BEGIN> ( statement() <SEMI_COLON> )* <END>
    | 	<IF> condition() <THEN> statement() <SEMI_COLON> <ELSE> statement()
    | 	<WHILE> condition() <DO> statement() | {}
}

void statementFac() #void : {} {
	<ASSIGNMENT> expression() | <LBR> arg_list() <RBR>
}

void expression() #void :{} {
	( identifier() expressionFac() | <TRUE> | <FALSE>| number()| ((<PLUS> | <MINUS>) fragment())
	| <LBR> expression() <RBR>) expressionPrime()
}

void expressionPrime() #void : {} {
    (<PLUS> | <MINUS> | <MULTIPLY> | <DIVIDE>)fragment() expressionPrime() | {}
}

void expressionFac() #void : {} {
      <LBR> arg_list() <RBR> | {}
}


void fragment() #void : {} {
    ( identifier() expressionFac()
    | ((<PLUS> | <MINUS>) fragment()) fragmentFac()
   	|   <LBR> expression() <RBR>)
    | 	<TRUE>
    | 	<FALSE>
    | 	<NUMBER>
}

void fragmentFac() #void : {} {
    expressionPrime()
}

void condition() #void : {} {
   ( <NOT> condition()
    | LOOKAHEAD(3)(identifier() expressionFac() | <TRUE> | <FALSE>| number()| ((<PLUS> | <MINUS>) fragment())| <LBR> expression() <RBR>) expressionPrime() (<EQUALS> | <NOT_EQUALS> | <LESS_THAN> | <GREATER_THAN> | <LESS_THAN_OR_EQUAL> | <GREATER_THAN_OR_EQUAL>) expression()
    | identifier() <LBR> arg_list() <RBR>) conditionPrime()
}
void conditionPrime() #void  : {} {
	( (<AND> | <OR>) condition() conditionPrime()) | {}
}

void ident_list() #void : {} {
    identifier() ( <COMMA> identifier() )*
}

void arg_list() #void : {} {
     (identifier() ( <COMMA> identifier() )* | {})
}






